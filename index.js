//Declare variable to run game BEGIN
let myVar;//SetIntervalnya
var i = -1;//Decide which question to take
let numQ = 5;//Number of questions in one game
var nilai= [0,0,0,0,0,0,0,0,0,0,0,0];//Score for players
var pName= ["","","","","","","","","","","","",""];//Nama Pemain
var ansd = ["","","","","","","","","",""];
var timerReset = 20 //Time to change to next question
var timer = timerReset;//Timer
var playerList = 1;//Number of player
var playerLimit = 10;//Limit of player
var roomState = false;//When the game is ongoing or not
var correctAnswers = 0;//Correct answers
var ansdIdx = 0;
var ansdBool = false;
//Pertanyaan
var pertanyaanlist = [
    
    {pertanyaan : "Disebut apa karya ilmiah yang bisa dijadikan sebagai sumber acuan dalam penerapan ilmu pengetahuan",
    jawaban : ["Artikel", "Skripsi", "Tesis"]},
    
    {pertanyaan : "Unsur yang didapat dari resensi buku adalah",
    jawaban : ["Judul", "Identitas", "Sinopsis", "Penilaian"]},
    
    {pertanyaan : "Berdasarkan jenis makanannya. Penggolongan hewan dibagi menjadi 3 jenis",
    jawaban : ["Karnivora", "Herbivora", "Omnivora"]},
    
    {pertanyaan : "Sebutkan hewan berkaki 8",
    jawaban : ["Laba-laba", "Tungau", "Caplak", "Kalajengking"]},
    
    {pertanyaan : "Sebutkan hewan berkaki sepuluh",
    jawaban : ["Kepiting", "Ketam", "Udang"]},
    
    {pertanyaan : "Apa saja sifat-sifat benda",
    jawaban : ["Padat", "Cair", "Gas"]},
    
    {pertanyaan : "Apasaja macam-macam bentuk energi yang ada",
    jawaban : ["panas", "cahaya", "gerak", "listrik", "bunyi", "kimia"]},
    
    {pertanyaan : "Sebutkan jenis-jenis fauna yang ada di kawasan Ethiopian",
    jawaban : ["Jerapah", "Gorila", "gajah", "zebra", "antilop", "unta"]},
    
    {pertanyaan : "Sebutkan gelaja-gelaja yang memengaruhi kekuatan gejala biosfer",
    jawaban : ["litosfer", "atmosfer", "hidrosfer"]},
    
    {pertanyaan : "Berikan contoh hewan-hewan yang mengalami metamorfosis sempurna adalah",
    jawaban : ["Kupu-kupu", "Katak", "Nyamuk", "lalat"]},
    
    {pertanyaan : "Berikan contoh hewan-hewan yang mengalami metamorfosis tidak sempurna adalah",
    jawaban : ["Kecoa", "Jangkrik", "Belalang"]},
    
    {pertanyaan : "Hewan iksektivora adalah hewan yang memakan serangga adalah",
    jawaban : ["Cicak", "Tokek", "bunglon"]},
    
    {pertanyaan : "Sebutkan jenis - jenis jaringan computer",
    jawaban : ["LAN", "WAN", "MAN", "Internet", "WLAN"]}, 
    
    {pertanyaan : "Sebutkan perangkat keras jaringan",
    jawaban : ["HUB", "Switch", "Bridge", "Router", "modem"]}, 
    
    {pertanyaan : "Sebutkan Jenis - jenis topologi jaringan",
    jawaban : ["Bus", "Star", "Ring", "Tree"]}, 
    
    {pertanyaan : "Sebutkan film mengenai ikan hiu?",
    jawaban : ["jaws", "sharks", "deep blue sea", "open water", "shark tale"]}, 
    
    {pertanyaan : "Sebutkan bahan bakar yang berasal dari alam",
    jawaban : ["Bensin", "Solar", "Batubara", "Minyak tanah", "Lpg"]},
    
    {pertanyaan : "Sebutkan unsur-unsur peta",
    jawaban : ["Judul", "garis tepi peta", "penunjuk arah","skala peta", "garis astronomis", "inset", "legenda", "simbol"]},
    
    {pertanyaan : "Contoh lingkungan fisik adalah",
    jawaban : ["Tanah", "Air", "Iklim"]},
    
    {pertanyaan : "Varenius membagi geografi  menjadi dua, yaitu",
    jawaban : ["umum", "khusus"]},
    
    {pertanyaan : "Sebutkan empat prinsip geografi",
    jawaban : ["Persebaran", "interrelasi", "Deskripsi", "korologi"]},
    
    {pertanyaan : "Berdasarkan bahan pencemarannya, pencemaran dibedakan menjadi tiga macam, yaitu",
    jawaban : ["Darat", "Air", "Udara"]},  
    
    {pertanyaan : "Konsep ekoefisiensi yang dapat diterapkan dalam pengolahan limbah meliputi",
    jawaban : ["Reduce", "Recycle", "Reuse"]},  
    
    {pertanyaan : "Kayu meranti dan berbagai jenis anggrek merupakan jenis flora khas daerah",
    jawaban : ["Sumatera", "Kalimantan"]},  
    
    {pertanyaan : "Neraca pembayaran internasional terdiri atas neraca.....",
    jawaban : ["berjalan", "saldo", "moneter"]},  
    
    {pertanyaan : "Neraca berjalan terdiri atas neraca......",
    jawaban : ["barang", "jasa"]}
    ];

var jumlahPertanyaan = pertanyaanlist.length;
//Pertanyaan END
//END





//Game function
//Setup
function stopFunction() {
    clearInterval(myVar);
    playerList = 1;
    i=-1;
    roomState = false;
    numQ = 5;
    nilai= [0,0,0,0,0,0,0,0,0,0,0,0,0];
    pName= ["","","","","","","","","","","","",""];
}

var socket = require('socket.io'),
    http = require('http'),
    server = http.createServer(),
    socket = socket.listen(server);
 
server.listen(3000, function(){
    console.log('Server started');
});

socket.on('connection', function(connection) {
    console.log('User Connected '+playerList.toString());
    socket.emit('playerNumber', playerList);
    playerList++;
    if(playerList>playerLimit)playerList=1;

    connection.on('message', function(msg){
        socket.emit('message', msg);
    });
    connection.on('roomList', function(){
        RoomList();
        
    })
    connection.on('gameState',function(state){
        if(state == "stop" && roomState == true) 
        {
            stopFunction();
            
        }
        if(state == "start" &&roomState == false)
        {
            roomState = true;
            myVar = setInterval(function(){gameLoop()}, 1000);
            
        }
    });
//SETUP END


//User jawab BEGIN
    connection.on("jawaban", function(jawaban, playerId, roomId, userName){
        var str = jawaban;
        
        if(str!=null)
        str = str.toLowerCase();
        console.log(ansd.length);

        for(var ii=0; ii<ansd.length;ii++){
            console.log(ansd[ii]);
            if(ansd[ii] == str){
                ansdBool = true;
                
                break;

            }
            else{
                ansdBool = false;
            }
        }

        var msg = userName + " : "+ jawaban;
        socket.emit('message'+roomId, msg);
        if(ansdBool==false){
            for(var ii = 0; ii<pertanyaanlist[i].jawaban.length;ii++){
                if(pertanyaanlist[i].jawaban[ii].toLowerCase() == str){
                    nilai[playerId] += str.length *100;
                    correctAnswers--;
                    pName[playerId] = userName + " = "+ nilai[playerId];
                    socket.emit('answers'+roomId, str);
                    ansd[ansdIdx] = str;
                    ansdIdx++;
                    socket.emit('score'+roomId, pName);
                    socket.emit('correct'+roomId, playerId);
                    break;
                }
                
            }
        }
        
    });

});
//User Jawab END


//GameLoop BEGIN
function gameLoop(){
    soal();
    timer--;
    if(timer<=0 || correctAnswers == 0){
        timer=timerReset;
        numQ--;
    }
}
//GameLoop END



//Soal BEGIN
function soal(){
    if(pertanyaanlist[i]==undefined){
        i=0;
        nilai= [0,0,0,0,0,0,0,0,0,0,0,0,0];
        timer=timerReset;
    }
    else if(numQ<=0){
        socket.emit('finish', nilai);
        socket.emit('finish2', pName);
        stopFunction();
        
    }
    else if(timer==timerReset){
        var temp = i;
        i = Math.floor((Math.random() * jumlahPertanyaan));
        if(i==temp && i>jumlahPertanyaan){i--;}
        else if(i==temp && i<jumlahPertanyaan){i++;}
        console.log(i + " temp="+temp);
        socket.emit('quiz', pertanyaanlist[i].pertanyaan);
        correctAnswers = pertanyaanlist[i].jawaban.length;
        ansdIdx = 0;
        socket.emit('nextQ', "temp");
    }
    else{
        socket.emit('quiz', pertanyaanlist[i].pertanyaan);
    }
    
}
//Soal END